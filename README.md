# Documentation for the HIFIS Software Consulting

This repository contains the documentation for all aspects of the HIFIS Consultants.

## Contents

The content is stored in the `docs` directory.
It is divided into a handbook and resource section.
The handbook section holds all information related to the overall consulting process.
The resources section serves as a collection of handout materials for clients.

1. [Documentation overview](docs/index.md)
    1. [Consultancy Service Handbook](docs/handbook/index.md)
    2. [Consulting Resources](docs/resources/index.md)

## Static Website Generator

The `docs` directory is automatically rendered into an HTML website with MkDocs and deployed to gitlab pages. 

### Review Websites

For all other branches, websites are generated from the most recent pipeline.
The websites can be accessed via the `View App` button in the merge request related to a specific branch.

The access to all review websites is restricted. Please ask a fellow consultant in order to retrieve the credentials.

## Conventions

* Markdown is the common format for the documentation. For the general syntax see https://www.markdownguide.org/basic-syntax
* MkDocs is used to render the HTML website. You can find the documentation here: https://www.mkdocs.org/#mkdocs
* Please use merge requests for all changes that significantly change one or more documents.

