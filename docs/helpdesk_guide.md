---
title: Helpdesk Guide
---

# Helpdesk Guide

An issue tracking and ticket system to manage and maintain lists of issues and requests is the single source of truth of consulting process.
It forms the control centre to monitor, document and prioritise different consulting tickets.
[Zammad](https://zammad.com/) is an Open Source helpdesk or issue tracking system used by HIFIS, not just in Consulting services.
These chapters explain some best practices and sets a context to effectively use Zammad for consulting bookkeeping purposes.
However, the recommendations can be considered generic and could be transferable across any ticketing platform.

## HIFIS Helpdesk

An instance of Zammad, called the _HIFIS Helpdesk_ is hosted and maintained by HIFIS.  
Since the tickets in the platform can be grouped, this feature can be used to classify and redirect tickets based on their target purpose.
For example use a _HIFIS Software Consulting Service_ group for software consultation tickets.
Since the platform can be used across different teams this ensures that consultants only see relevant tickets.

## General Guidelines

Since Zammad is considered as the single source of truth of the consulting services it is important to keep the tickets up-to-date.
Here are a few guidelines to follow while working on a consulting ticket:

1. Communication should either be through the platform, or written up in the platform after it happens. Video calls, face-to-face conversations, chats, etc. should be summarised in the platform.
2. Prefer using notes over emails in Zammads interface to record or summarize. Sometimes emails may be necessary because notes cannot be sent to multiple clients or because attachments with notes seem to not get sent.
3. Use the private-public switch in the Zammad notes effectively for internal assessments and discussions.
4. **Be aware of private vs. public comments**. By default, comments are private, and you need to click the button on the side to create a public comment. If you create a private comment and _then_ make it public. The client will not get notified, so instead delete that comment, and recreate it. At best make it public from the start. You know if the client got an email because email communications in tickets are always written down.
5. Zammad marks the status changes of the ticket in the ticket history. It can be found on the top right pane.

## Tags

The following tags are (pre)defined in the helpdesk to annotate the status of every ticket:  

1. `consulting-survey-pending`
2. `consulting-survey-sent`
3. `no-survey`
4. `report-pending`
5. `report-done`

Consultants are expected to tag the tickets appropriately with the aforementioned tags. Additional tags may be created and used as per ticket needs.
