---
title: Consultants
---

# Consultants

Consultants are the primary/first-level respondents to a consulting request.
A consultant coordinates meetings, understands the requirements in the given scope and delivers technical advice.
If necessary, the consultant can call for in-depth knowledge of a domain expert.

## Hierarchy

The hierarchy at the HIFIS Consulting is flat. This implies two directive principles: nobody "reports to" anybody else OR everybody reports to everyone mutually.
Hence, the style of implementation of the principle is decided and changed by mutual consensus. Flat hierarchy enables faster and empowered decision-making
for a consulting ticket. There is no red tape stopping from figuring out for oneself what the clients want in a consulting, and then providing it to them.

## Onboarding for Consultants

While welcoming a new member to the Consulting workgroup, it is important to get to know essentials of the existing consulting infrastructure.
These form the vital virtual pillars of the consulting setup.
They are the cornerstones facilitating seamless coordination in the consultancy across the consultants and deliver consulting to beneficiaries.
The following list presents essential online sites to which a consultant must have access and/or permissions.
It rests upon the newly joined consultant to push for attaining useful information on the appropriate usage of the following sites.
This handbook will also try to cover them in the following chapters.

### Important Sites

1. Chat service *Mattermost*: The [Mattermost](https://mattermost.com/) is the HIFIS text-based collaborative chat platform for instant messaging and group discussions. It is used for communication within the consulting group and on the client-side utilising dedicated channels.
2. Issue tracking system *Zammad*: An issue tracking system/helpdesk software and interface forms the single point of truth for every consulting workgroup. HIFIS Helpdesk is built on [Zammad](https://en.wikipedia.org/wiki/Zammad).
3. File storage cloud service *"Nubes"*: Nubes is a [NextCloud](https://en.wikipedia.org/wiki/Nextcloud) instance for data/file storage. HIFIS Consulting maintains list of experts, consultants; their corresponding skill set and other files 
related to consulting like meeting minutes etc. here.
4. Version control and DevOps platform *"Gitlab"*: HIFIS hosts a [Gitlab](https://en.wikipedia.org/wiki/GitLab) instance for all HIFIS-related code repositories to effectively communicate via code to clients in a sustainable manner.
