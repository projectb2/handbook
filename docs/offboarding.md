---
title: Offboarding
---

# Offboarding

Offboarding is the process that leads to the formal separation between an employee and a company through resignation or termination.
It encompasses all the decisions and processes that take place when an employee leaves. This may include the aspects listed below.

## Knowledge Transfer

Transferring the exiting consultant's job responsibilities to another consultant is important before they leave the group. It is highly
recommended for the exiting consultant to announce their tenure end in advance and remove oneself from the consulting commitments.
This includes completion of consulting reports for concluded consultations, re-assigning and handing over ongoing consultations in helpdesk to fellow
consultants, and knowledge transfer with client's consent.  
It is the responsibility of the stakeholder to make sure that happens.

## Deactivating access rights and passwords

The stakeholder shall remove rights of the exiting consultant across different sites like Mattermost, helpdesk,
Nubes, etc. This may be accomplished by removing necessary privileges authentication services.
Again, it is the responsibility of the stakeholder to do so, especially when the exiting consultant leaves but still will remain
part of the organisation.

## Exit interview

If the employee leaves at will, the stakeholder shall conduct an exit interview to gather feedback. The details of the interview should be
kept confidential unless otherwise required.
