---
title: Consulting Guide
---

# Consulting Guide

This chapter elaborates the consulting workflow as defined by HIFIS Consulting services.
This workflow should be understood and followed by every consultant for consistency and reproducibility purposes.

## Consulting Process

The workflow is created in an dynamic manner.
That is, the process starts minimal and extents, if necessary.
The basic overview is given in the image below.

![Consulting workflow](./images/consulting-workflow.svg)

Assuming [Zammad](helpdesk_guide.md) is the ticket platform used, the following process is defined for consulting:

1. A client requests a consultation via:
    - a request form on the website (e.g. [LimeSurvey](https://www.limesurvey.org/))
    - a direct email to the [helpdesk](helpdesk_guide.md).

2. A ticket is created in the [helpdesk](helpdesk_guide.md) followed by an initial assessment of the request. The consulting team or moderator 
    1. Reads the ticket.
    2. Optionally, checks back with additional questions via the helpdesk.
    3. Assigns a consultant and/or experts based on their competence and/or available resources.
    4. Discusses the ticket and initial internal assessment of the request.
    5. Changes the state of the ticket from `New` to `Open`.
    6. Decides whether the consultation needs to be declined or is continued.

3. Consultation (see section [Consulting procedures](#consultation-procedures))
    1. [Initial invite](#initial-invite) via the ticket system to a first meeting.
    2. [First consultation meeting](#first-consultation) organised virtually as video conference or in person.
    3. Regular follow-up consultations depending on demand.

4. Reporting; Once a consulting is completed (See chapter [Consulting reporting](consulting_reporting.md)). The consulting team or moderator
    1. Changes the Zammad ticket state to `Close`.
    2. Annotates the `report-pending` tag to Zammad ticket.
    3. Prepares the consulting report YAML file and adds the file to the consulting report [repository](https://gitlab.hzdr.de/hifis/software/consulting/consulting-reports-template).
    4. Changes the ticket tag to `report-done`, once the merge is accepted.

5. Post-consulting survey; Once a consulting is completed (see chapter [Post Consulting Survey](post_consulting_survey.md)). The consulting team or moderator
    1. Closes the ticket on the helpdesk and assigns the `consulting-survey-pending` tag.
    2. Ensures the anonymity of participants, by sending the LimeSurvey link only when there are at least 10 recipients across all consulting tickets.
    3. Sends the link via the helpdesk (public note or email) to the client(s) and changes the tag to `consulting-survey-sent`.
    4. Changes the tag to `no-survey` in case a consultation is deemed not eligible for collecting feedback.

All communication with the client shall be carried out via Zammad, if possible.
Consulting related documents shall be stored in the Nubes cloud.

## Consultation Procedures

The main goal of every consultation should be to enable the client to address their problems and to provide technical assistance to their concerns.
The scope of the Consulting process includes supporting scientists with their technical decisions, to support with project-specific issues and setting up a development process with a focus on sustainable research software engineering.

### Initial Invite

Offer a video call in the first email via Zammad.
In our experience, clients often have not fully explained themselves in the initial request and talking directly will then help to clarify things.
Don't guess what the solution will look like until that video call - it probably won't be right!
**Use formal language unless clearly indicated otherwise in the incoming email. In the inital meeting, you can ask if relatively informal language can be used.**
See the [sample template](resources/template-welcome-note.md) of the initial invite email. This can be added to Zammad as a template for reuse.

### First Consultation

The first meeting is mainly to elaborate the client's request depending on the topic. **Don't offer hasty solutions to the client in the initial video call unless it is very clear what is needed.**
Consultants and experts can clarify more on the client's first request email to understand the needs and plan the consulting effort and personnel required for the issue.
If effort cannot be immediately estimated from the initial consultation, postpone the decision and discuss with fellow consultants or request help from other experts on the ticket.
To prevent misunderstandings about the motivation of the client, action points may be defined in order to reach a common agreement on what is in the scope of the consulting and not.

The definition of action points and noting down other important details of the consultation may be done with the help of a collaborative document e.g. Google Docs, HedgeDoc, Etherpad etc.
HIFIS Consulting uses [HedgeDoc](https://notes.desy.de) as the platform to write and share markdown documents with the people involved in the consulting.
This increases transparency and serves as a general anchor to connect client and consultant.
See the [sample template](resources/template-consulting-pad.md) of a markdown file used for collaboration.

If the request is found out of scope or no matching consultant or expert is found, the consultation can be discontinued.
This implies that the post-consulting survey need not be sent to the client.
However, a post-consulting report has to be created.

### Follow-up Consultation

When the consulting goes as smoothly as planned, usually more meetings are scheduled for further clarification of the problem and solutions at hand.
The client should be informed about the [Publication Policy](publication_policy.md) and consent should be given.
Also, the client should be informed that information about the consultation will be shared between consultants in the team.
Consultants and experts can use _States_ in Zammad. The states `pending reminder` & `pending close` are useful to set timelines for achieving milestones or to change the state of ticket from `open` if waiting for feedback from a client.

## Consulting Types

In order to provide good consultation, HIFIS Consulting has defined different types of consulting requests.
This allows us to provide type specific recommendations regarding how to do a consultation as well as fitting resources.
More importantly, this categorisation helps in better bookkeeping and analysis. Read the chapter on [Post-consultation report](consulting_reporting.md) on how to report a consulting properly.
The table of request types might change over time as new request types will be added and others will be split up.

| Request type                               | Topic                | Examples                                                                                   | Ref.         |
|--------------------------------------------| ------               | ------                                                                                     |--------------|
| Qualification                              | Software Engineering | Ensure that persons involved in the development have the necessary knowledge and training  | [[1][DLRSE]] |
| Requirements Management                    | Software Engineering | Problem definition, functional requirements, quality requirements, constraints             | [[1][DLRSE]] |
| Software Architecture                      | Software Engineering |                                                                                            | [[1][DLRSE]] |
| Change Management                          | Software Engineering | How to document and performing changes to software in a systematic and comprehensible way  | [[1][DLRSE]] |
| Design and Implementation                  | Software Engineering |                                                                                            | [[1][DLRSE]] |
| Software Test                              | Software Engineering | Define a test strategy and which kinds of tests are useful                                 | [[1][DLRSE]] |
| Release Management                         | Software Engineering | Release number, release notes, release performance                                         | [[1][DLRSE]] |
| Automation and Dependency Management       | Software Engineering |                                                                                            | [[1][DLRSE]] |
| Software or Programming Language Migration | Practical            | Matlab Code to C++                                                                         |              |
| Code Audit                                 | Practical            | Source code audit, project structure, software architecture                                |              |
| Technology and Tool recommendations        | Practical            |                                                                                            |              |
| Infrastructure Support                     | Practical            | VCS setup, Docker, CI/CD, internal HIFIS support                                           |              |
| Licenses                                   | Legal                | Usage of licenses                                                                          |              |
| Open Source                                | Legal                | Usage and publication of (open source) software                                            |              |
| Patent                                     | Legal                | Patent law                                                                                 |              |
| Contract suggestions                       | Legal                |                                                                                            |              |

### References

[1] T. Schlauch, M. Meinel, C. Haupt, "DLR Software Engineering Guidelines", Version 1.0.0, August 2018. Available: [https://doi.org/10.5281/zenodo.1344612][DLRSE]

<!-- LINKS -->
[DLRSE]: https://doi.org/10.5281/zenodo.1344612
