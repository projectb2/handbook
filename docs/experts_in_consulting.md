---
title: Experts in Consulting
---

# Experts in Consulting

Experts are people who are willing to support the [consultants](consultants.md).
An expert is invited to be involved in consultation if no consultant is available or, the expert has a matching skill set or, is familiar with the organisation where the request is originated from.
The chapter focuses on the process of onboarding and inviting an expert.

## Onboarding for Experts

Since the experts are invited into a consulting by consultants or other stakeholders, it is recommended for a consulting team to prepare a list of people with corresponding skill set, beforehand.
This list,  could be a spreadsheet containing experts' names, skill sets, contacts etc. and has to be prepared with consent of the experts involved.
Ideally, this list should be accessible by the experts involved to update or remove their corresponding skill set over time.
Contrary to consultants, an expert does not need access to all sites like Mattermost and Nubes. However, it is highly recommended that an expert has access to the helpdesk to keep track of the consulting ticket progress, facilitating seamless involvement.

## Expert Inclusion Workflow

If no consultant is available for a consultation (e.g. due to none matching the required skill set) you can get an expert on board, following this process:
 
* Pick a matching expert from the above-mentioned expert-list based on these factors (in descending order):
    1. Matching skill set (Check the "Skills" column)
    2. Priority is given to those that are 
	    1. Already on Zammad
		2. Related to consulting group
		3. Listed with up-to-date contact info (in descending order) (see tab "Staff")
    3. Part of same team, group, or organisation as the client

* Contact via main contact of the team or organisation and ask for availability.
    * If available but not on Zammad: Include the expert in consultation process via email. Stay on for organising communication per email and support. Make sure that all external communication is documented in Zammad.
    * If an expert is already on Zammad: Assign issue to expert, but ask the consultant to subscribe to the issue to be kept in the loop.
    * Remind the expert of the rules of communication and bookkeeping in Zammad.
    * If not available: Start over.
